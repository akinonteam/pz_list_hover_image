# pz_list_hover_image
This application allows the pictures to change in accordance with the position of the mouse in multi pictured products.

![Example](./exp.gif)

## Usage
### 1. Install the app

At project root, create a requirements.txt if it doesn't exist and add the following line to it;

```bash
-e git+https://git@bitbucket.org/akinonteam/pz_list_hover_image.git
```

For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.

```bash
# in venv
pip install -r requirements.txt
```
### 2. Install the npm package

```bash
# in /templates
yarn add ​git+ssh://git@bitbucket.org:akinonteam/pz_list_hover_image.git
```

Make sure to use the same git commit id as in `requirements.txt`.

### 3. Add to the project

```python
# omnife_base/settings.py:

INSTALLED_APPS.append('pz_list_hover_image')
```
### 4. Import template:

```jinja
/**
* BASIC TEMPLATE IMPORT
*
* 1- Open ```templates/components/product-item/index.html```
* 2- Add ```{% from 'pz_list_hover_image/index.html' import ListHoverImage with context %}``` this line to head of file.
* 3- Remove div element with class product-item__image and add ```{{ ListHoverImage() }}```
*
*/

/**
 * ListHoverImage class takes two parameter.
 * 
 * First one is the thumbnailOptions. 
 * Default is [
    { '(min-width:1370px)': 'product-list-layout-{}-xl' },
    { '(min-width:1170px)': 'product-list-layout-{}-lg' },
    { '(min-width:768px)': 'product-list-layout-{}-md' },
    { '(min-width:425px)': 'product-list-layout-{}-s' },
    { 'default': 'product-list-layout-{}-xs' }
  ]
 * 
 * Second one is the useDynamicLayout.
 * Default is true.
 * */

// It's uses default thumbnailOptions and useDynamicLayout.
{{ ListHoverImage() }}

/**
 * If you want use thumbnailOptions with dynamic layout.
 * You should add `{}` to every key of thumbnailOptions
 * 
 * For example:
 * {
    ...
    '(min-width:768px)': 'my-layout-{}-thumb-option',
    ...
 * }
*/

/**
 * Whether you use static or dynamic layout,
 * you must enter a default value.
 * 
 * Default option uses as fallback.
 * 
 * Example of dynamic layout.
*/
{{ ListHoverImage(thumbnailOptions=[
  { '(min-width:1370px)': 'my-dynamic-layout-{}-xl' },
  { '(min-width:768px)': 'my-dynamic-layout-{}-md' },
  { 'default': 'my-dynamic-layout-{}-xs' }
]) }}

/**
 * If you want use static layout, second parameter of class should false.
 * 
 * Example of static layout.
*/
{{ ListHoverImage(thumbnailOptions=[
  { '(min-width:1370px)': 'my-static-layout-xl' },
  { '(min-width:768px)': 'my-static-layout-md' },
  { 'default': 'my-static-layout-xs' }
], useDynamicLayout=false) }}
```

### 5. Import and initialize JS

```js
// In ```templates/components/product-item/index.js``` file

import ListHoverImage from 'pz-list-hover-image';

// ...
constructor() {
  // ...
  new ListHoverImage();
}
```

### 6. Import styles:

```scss
@import "~pz-list-hover-image/";
```

## Template settings
```jinja
{# Defaults #}

{{
  ListHoverImage(thumbnailOptions=[
    { '(min-width:1370px)': 'product-list-layout-{}-xl' },
    { '(min-width:1170px)': 'product-list-layout-{}-lg' },
    { '(min-width:768px)': 'product-list-layout-{}-md' },
    { '(min-width:425px)': 'product-list-layout-{}-s' },
    { 'default': 'product-list-layout-{}-xs' }
  ], useDynamicLayout=true)
}}
```
All of the following are optional parameters for the Jinja2 macro.
* __thumbnailOptions__: (Array) A general thumbnail options.
* __useDynamicLayout__: (Boolean) That generates thumbnail url according to current layout. 