const { thumbnail } = require('shop-packages/utils');

class ListHoverImage {
  constructor() {
    this.imageSelector = '.js-list-hover-image';
    this.dotsWrapperSelector = '.js-list-hover-image-dot-wrapper';
    this.images = document.querySelectorAll(this.imageSelector);
    this.activeIndex = 0;
    this.init();
  }

  init() {
    this.bindEvents();
  }

  bindEvents() {
    this.images.forEach((image) => {
      image.addEventListener('mouseenter', this.handleMouseEnter);
      image.addEventListener('mousemove', this.handleMouseMove);
      image.addEventListener('mouseleave', this.handleMouseLeave);
    });
  }

  createBreakpoints = (length) => {
    let breakpoints = [];
    const threshold = Math.round(this.elementWidth / length * 100) / 100;

    for (var i = 0; i < length; i++) {
      breakpoints.push([i * threshold, (i + 1) * threshold]);
    }

    return breakpoints;
  };

  calculateActiveIndex = (xPos, breakpoints) => {
    if (xPos < 0) { return 0; }

    if (xPos > this.rect.left) { return breakpoints.length - 1; }

    return breakpoints.findIndex((points) => xPos >= points[0] && xPos < points[1]);
  };

  changeImage = () => {
    const activeDot = this.dotsWrapper.querySelector('.js-image-dot.-active');

    activeDot?.classList.remove('-active');
    this.dots[this.activeIndex]?.classList.add('-active');

    if (this.source) {
      this.source.srcset = thumbnail(this.imagesData[this.activeIndex], this.source.getAttribute('data-thumb-option'));
    } else {
      const img = this.picture.querySelector('img');
      img.src = thumbnail(this.imagesData[this.activeIndex], img.getAttribute('data-thumb-option'));
    }
  }

  reset = () => {
    this.activeIndex = 0;
    this.changeImage();
  }

  handleMouseEnter = (e) => {
    const target = e.currentTarget;
    const imageLength = Number(target.getAttribute('data-image-length') || 1);

    this.layout = target.getAttribute('data-layout') || 3;
    this.imagesData = JSON.parse(target.getAttribute('data-images'));
    this.dotsWrapper = target.querySelector(this.dotsWrapperSelector);
    this.picture = target.querySelector('picture');
    this.dots = this.dotsWrapper.children;
    this.rect = target.getBoundingClientRect();
    this.elementWidth = target.offsetWidth;
    this.breakpoints = this.createBreakpoints(imageLength);

    const sources = this.picture.querySelectorAll('source');

    this.source = Array.from(sources).find((item) => window.matchMedia(item.media).matches);
    this.dotsWrapper?.classList.add('-visible');
  }

  handleMouseMove = (e) => {
    if (this.dots.length > 1) {
      const xPos = e.clientX - this.rect.left;
      const index = this.calculateActiveIndex(xPos, this.breakpoints);

      if (index !== this.activeIndex) {
        this.activeIndex = index;
        this.changeImage();
      }
    }
  }

  handleMouseLeave = () => {
    this.reset();
    this.dotsWrapper?.classList.remove('-visible');
  }
}

export default ListHoverImage;
